//
//  FeedModel.swift
//  ShotSpeak
//
//  Created by Sergiy Bekker on 15.11.17.
//  Copyright © 2017 SBApps. All rights reserved.
//

import Foundation
import SwiftyJSON

struct FeedCategory {
    var id : Int?
    var name : String?
    var subscribe : String?
    
    init(_ json : [String : JSON]) {
        self.id = json["id"]?.rawValue as? Int
        self.name = json["name"]?.rawValue as? String
        self.subscribe = json["subscribe"]?.rawValue as? String
    }
}

struct FeedModel {
    
    var id : Int?
    var like_count : Int?
    var liked : Int?
    var image : String?
    var text : String?
    var distance : Int?
    var cdate : String?
    var comment_count : Int?
    var category : FeedCategory
    var favorite : Int?
    var next : String?
    var isMore : Bool! = false
    
    init(_ json : [String : JSON], _ next : String?, _ index : Int) {
        self.id = json["id"]?.rawValue as? Int
        self.like_count = json["like_count"]?.rawValue as? Int
        self.liked = json["liked"]?.rawValue as? Int
        self.image = json["image"]?.rawValue as? String
        self.text = json["text"]?.rawValue as? String ?? ""
        if index % 2 > 0 {
            self.text = self.text! + Constants().addText
        }
        self.distance = json["distance"]?.rawValue as? Int ?? 0
        
        if json["cdate"] != nil {
            var dateString = json["cdate"]?.rawValue as! String
            var separated = dateString.components(separatedBy: ".")
            dateString = separated[0]
            dateString = (dateString as NSString).replacingOccurrences(of: "T", with: " ")
            separated = dateString.components(separatedBy: " ")
            dateString = separated[0]
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            dateFormatter.locale = Locale(identifier: "en_GB")
            let dateObj = dateFormatter.date(from: dateString)
            dateFormatter.dateFormat = "dd.MM.yyyy"
            self.cdate = dateFormatter.string(from: dateObj!)
        }
        self.comment_count = json["comment_count"]?.rawValue as? Int
        self.favorite = json["favorite"]?.rawValue as? Int
        self.next = next
        self.category = FeedCategory([:])
        if json["category"] != nil {
            self.category = FeedCategory((json["category"]?.dictionaryValue)!)
        }
    }
}
struct FeedView {
    
    var feedData : [FeedModel] = []
    
    init(_ json : JSON) {
        let next = json["next"].rawValue as? String
        let data = json["results"].arrayValue
        let temp = NSMutableArray()
        for i in 0..<data.count {
            let obj = data[i]
            temp.add(FeedModel(obj.dictionaryValue as [String : JSON],next,i))
        }
        self.feedData = temp as! [FeedModel]
    }
}
