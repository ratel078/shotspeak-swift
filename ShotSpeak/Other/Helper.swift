//
//  Helper.swift
//  DiffcoTest
//
//  Created by Sergiy Bekker on 06.11.17.
//  Copyright © 2017 SBApps. All rights reserved.
//

import UIKit
import SystemConfiguration
import SDWebImage
import Toucan


// MARK: NSObject

extension NSObject {

    func loadImagewithUrl( _ url : String?, _ target : UIImageView , _ size : CGSize){
        
        let manager:SDWebImageManager = SDWebImageManager.shared()
        let requestURL:NSURL = URL(string:url!)! as NSURL
        manager.loadImage(with: requestURL as URL, options: SDWebImageOptions.highPriority, progress: { (start, progress, url) in
            
        }) { (image, data, error, cached, finished, url) in
            if (error == nil && (image != nil) && finished) {
                manager.saveImage(toCache: image, for: url!)
                DispatchQueue.main.async {
                   
                    var reSize = size
                    if size.height == 0 {
                        reSize.height = CGFloat(CellSizeType.FeedImageSize.rawValue)
                    }
                    let img = Toucan(image: image!).resize(reSize,  fitMode: Toucan.Resize.FitMode.crop).image
                    target.image = img
                    manager.saveImage(toCache: img, for: url!)
                }
            }
        }
    }


   public func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
    func calculateHeight(inString:String, width: CGFloat) -> CGFloat {
        let messageString = inString
        let attributes : [NSAttributedStringKey : Any] = [NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue) : UIFont(name: "Helvetica Neue", size: 16.0) as Any]
        
        let attributedString : NSAttributedString = NSAttributedString(string: messageString, attributes: attributes)
        
        let rect : CGRect = attributedString.boundingRect(with: CGSize(width: width, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
        
        let requredSize:CGRect = rect
        return requredSize.height
    }
    func boundWight() -> WightType {
        
        if UIDevice.current.is_iPhones_5_5s_5c_SE {
            return .Wieght_SE
        } else if UIDevice.current.is_iPhones_6_6s_7_8 {
            return .Wieght_8
        } else if UIDevice.current.is_iPhones_6Plus_6sPlus_7Plus_8Plus {
            return .Wieght_8Plus
        }
        return .Wieght_8Plus
    }
   
    func formatDate(_ param : String) -> String {
        
        var dateString = param
        var separated = dateString.components(separatedBy: ".")
        dateString = separated[0]
        dateString = (dateString as NSString).replacingOccurrences(of: "T", with: " ")
        separated = dateString.components(separatedBy: " ")
        dateString = separated[0]
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.locale = Locale(identifier: "en_GB")
        let dateObj = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = "dd.MM.yyyy"

        return dateFormatter.string(from: dateObj!)
    }
}

// MARK: UITableView

extension UITableView {
    func reloadData(completion: @escaping ()->()) {
        UIView.animate(withDuration: 0, animations: { self.reloadData() })
        { _ in completion() }
    }
    func reloadVisibleCells(_ indexPath : [IndexPath],completion: @escaping ()->())  {
        let indexPaths = self.indexPathsForVisibleRows
        if indexPaths!.count > 0 {
            UIView.setAnimationsEnabled(false)
            self.beginUpdates()
            self.reloadRows(at: indexPath, with: UITableViewRowAnimation.none)
            self.endUpdates()
            UIView.setAnimationsEnabled(true)
        }
        completion()
    }
}

// MARK: String

extension String {
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstraintedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    
    subscript(value: PartialRangeUpTo<Int>) -> Substring {
        get {
            return self[..<index(startIndex, offsetBy: value.upperBound)]
        }
    }
    
    subscript(value: PartialRangeThrough<Int>) -> Substring {
        get {
            return self[...index(startIndex, offsetBy: value.upperBound)]
        }
    }
    
    subscript(value: PartialRangeFrom<Int>) -> Substring {
        get {
            return self[index(startIndex, offsetBy: value.lowerBound)...]
        }
    }
    
}

// MARK: UIViewController

extension UIViewController {
    func alertMessage(_ title : String? ,_ message : String) {
       
        var _title = title
        if title == nil {
            _title = "Warning"
        }
        let alertController = UIAlertController(title: _title, message: message, preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
            
        }

        alertController.addAction(action1)
 
        self.present(alertController, animated: true, completion: nil)
    }
}

// MARK: UIDevice

extension UIDevice {
    var iPhone: Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }
    enum ScreenType: String {
        case iPhone4 = "iPhone 4 or iPhone 4S"
        case iPhones_5_5s_5c_SE = "iPhone 5, iPhone 5s, iPhone 5c or iPhone SE"
        case iPhones_6_6s_7_8 = "iPhone 6, iPhone 6S, iPhone 7 or iPhone 8"
        case iPhones_6Plus_6sPlus_7Plus_8Plus = "iPhone 6 Plus, iPhone 6S Plus, iPhone 7 Plus or iPhone 8 Plus"
        case iPhoneX = "iPhone X"
        case unknown
    }
    var screenType: ScreenType {
        switch UIScreen.main.nativeBounds.height {
        case 960:
            return .iPhone4
        case 1136:
            return .iPhones_5_5s_5c_SE
        case 1334:
            return .iPhones_6_6s_7_8
        case 1920, 2208:
            return .iPhones_6Plus_6sPlus_7Plus_8Plus
        case 2436:
            return .iPhoneX
        default:
            return .unknown
        }
    }
    var is_iPhone4: Bool {
        get {
            return UIDevice.current.screenType.rawValue == ScreenType.iPhone4.rawValue
        }
    }
    var is_iPhones_5_5s_5c_SE: Bool {
        get {
            return UIDevice.current.screenType.rawValue == ScreenType.iPhones_5_5s_5c_SE.rawValue
        }
    }
    var is_iPhones_6_6s_7_8: Bool {
        get {
            return UIDevice.current.screenType.rawValue == ScreenType.iPhones_6_6s_7_8.rawValue
        }
    }
    var is_iPhones_6Plus_6sPlus_7Plus_8Plus: Bool {
        get {
            return UIDevice.current.screenType.rawValue == ScreenType.iPhones_6Plus_6sPlus_7Plus_8Plus.rawValue
        }
    }
    var is_iPhoneX: Bool {
        get {
            return UIDevice.current.screenType.rawValue == ScreenType.iPhoneX.rawValue
        }
    }
}

// MARK: Notification

extension Notification.Name {
    static let feedMoreNotification = Notification.Name("feedMoreNotification")
    
    
}
