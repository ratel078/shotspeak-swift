//
//  Coordinator.swift
//  DiffcoTest
//
//  Created by Sergiy Bekker on 06.11.17.
//  Copyright © 2017 SBApps. All rights reserved.
//

import UIKit
import SwiftyJSON

struct Login {
    var email : String?
    var password : String?
    var re_password : String?
    var content : [String: Any]? {
        get {
            return ["email":self.email as Any,
                    "password":self.password as Any,
                    "re_password":self.re_password as Any]
        }
    }
    
    init() {
        self.email = "ratel078@gmail.com"
        self.password = "fazawaka"
        self.re_password = "fazawaka"
    }
}

class FeedPresenter: NSObject {

    static let sharedInstance = FeedPresenter()
    weak var feedView: FeedPresenterProtocol?
    
    func attachView(_ obj:FeedPresenterProtocol){
        self.detachView()
        feedView = obj
    }
    
    func detachView() {
        feedView = nil
    }
  
    

}

extension FeedPresenter: FeedControllerProtocol{
    func feedContent(_ nextUrl : String?){
        
        WebService.sharedInstance.requestData.next = nextUrl
        WebService.sharedInstance.requestFeed(Login().content!, completion: {(_ result: JSON) in
            let builder = modelBuilder()
            let obj = builder.createFeed(result) as FeedView
            self.feedView?.updateContent(obj)
            WebService.sharedInstance.requestData.next = nil
        })
        
    }
}
