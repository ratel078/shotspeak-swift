//
//  ActivityCell.swift
//  ShotSpeak
//
//  Created by Sergiy Bekker on 11.11.17.
//  Copyright © 2017 SBApps. All rights reserved.
//

import UIKit

class ActivityCell: UITableViewCell {

    @IBOutlet weak var indicator : UIActivityIndicatorView!
    @IBOutlet var topView : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        topView.tintColor = Constants().mainCollor
    }
    
    func startAnime() {
        self.indicator!.startAnimating()
    }
}
