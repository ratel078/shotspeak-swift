//
//  FeedCell.swift
//  ShotSpeak
//
//  Created by Sergiy Bekker on 11.11.17.
//  Copyright © 2017 SBApps. All rights reserved.
//

import UIKit
import QuickRippleButton

class FeedCell: UITableViewCell {

    @IBOutlet weak var textView : UITextView!
    @IBOutlet weak var categoryView : UILabel!
    @IBOutlet weak var distanceView : UILabel!
    @IBOutlet weak var timeView : UILabel!
    @IBOutlet weak var likeView : UILabel!
    @IBOutlet weak var commentView : UILabel!
    @IBOutlet weak var imgView : UIImageView!
    @IBOutlet weak var likeBut : QuickRippleButton!
    @IBOutlet weak var shareBut : QuickRippleButton!
    @IBOutlet weak var favBut : QuickRippleButton!
    @IBOutlet weak var moreBut : QuickRippleButton!
    @IBOutlet weak var imgHight : NSLayoutConstraint!
    @IBOutlet weak var textHight : NSLayoutConstraint!
    @IBOutlet weak var distanceWidth : NSLayoutConstraint!
    @IBOutlet weak var commentWidth : NSLayoutConstraint!
    @IBOutlet var topView : UIImageView!
    
    var index : Int!

    var content : FeedModel? {
        didSet{
            
            textView.text = nil
            distanceView.text = nil
            likeView.text = nil
            commentView.text = nil
            categoryView.text = nil
            self.imgView.image = nil
            
            if content != nil {
                
                if let text =  content?.text{
                    textView.text = text
                }
                if let distance =  content?.distance {
                    distanceView.text = "\(distance) км"
                }
                if let cdate =  content?.cdate{
                    timeView.text = cdate
                }
                if let like_count =  content?.like_count {
                    likeView.text = "\(like_count)"
                }
                if let comment_count =  content?.comment_count {
                    commentView.text = "\(comment_count)"
                }
                if let category_name =  content?.category.name{
                    categoryView.text = category_name
                }
                if let image =  content?.image {
                    self.loadImagewithUrl(image, self.imgView, self.imgView.frame.size)
                }
            }
        }
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if let text =  content?.text {
            let hieght = self.calculateHeight(inString: text,width:CGFloat(self.boundWight().rawValue))
            if hieght < CGFloat(CellSizeType.FeedTextSize.rawValue){
                self.textHight.constant = hieght + 20.0
            } else {
                self.textHight.constant =  (content?.isMore!)! ? hieght  : CGFloat(CellSizeType.FeedTextSize.rawValue)
            }
            self.moreBut.isHidden = hieght <= CGFloat(CellSizeType.FeedTextSize.rawValue)
        }
        if let distance =  content?.distance {
            let text = "\(distance) км"
            self.distanceWidth.constant = text.width(withConstraintedHeight: self.distanceView.frame.height, font: self.distanceView.font)
        }
        if let comment_count =  content?.comment_count {
            let comment = "\(comment_count)"
            self.commentWidth.constant = comment.width(withConstraintedHeight: self.commentView.frame.height, font: self.commentView.font)
        }
        self.imgHight.constant = CGFloat(CellSizeType.FeedImageSize.rawValue)
        if content?.image == nil {
            self.imgHight.constant = CGFloat(0.0)
        }
    }
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        topView.tintColor = Constants().mainCollor
        self.textView.isUserInteractionEnabled = false
    }
    

    @IBAction func clicked(sender : UIButton){
        
        if sender == self.moreBut{
            NotificationCenter.default.post(name: .feedMoreNotification, object: self.index)
        }
    }

}
