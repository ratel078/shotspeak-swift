//
//  SegmaenView.swift
//  ShotSpeak
//
//  Created by Sergiy Bekker on 12.11.17.
//  Copyright © 2017 SBApps. All rights reserved.
//

import UIKit
import QuickRippleButton

protocol SegmentDelegate : NSObjectProtocol {
    func selectItem(_ target : SegmentView, _ index : Int)
}

class SegmentView: UIView {

    @IBOutlet var buttons : [QuickRippleButton]!
    @IBOutlet var images : [UIImageView]!
    @IBOutlet var topView : UIImageView!
    weak var feedView: SegmentDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.clicked(buttons[0])
        topView.tintColor = Constants().mainCollor
    }
    
    func attachView(_ view:SegmentDelegate){
        feedView = view
    }
    
    func detachView() {
        feedView = nil
    }
    
    // MARK: Private methods
    
    @IBAction func clicked(_ sender : UIButton){
        self.resetSegment()
        self.selectSegment(sender)
    }
    
    fileprivate func selectSegment(_ obj : UIButton)  {
        var index = 0
        obj.setTitleColor(Constants().selectCollor, for: .normal)
        obj.setTitleColor(Constants().selectCollor, for: .highlighted)
        for i in 0..<images.count {
            let image = images[i]
            if image.tag == obj.tag {
                images[i].tintColor = Constants().selectCollor
                index = i
                break
            }
        }
        self.feedView?.selectItem(self, index)
    }
    
    fileprivate func resetSegment()  {
        
        for i in 0..<buttons.count {
            buttons[i].setTitleColor(Constants().unselectCollor, for: .normal)
            buttons[i].setTitleColor(Constants().unselectCollor, for: .highlighted)
            images[i].tintColor = UIColor.white
        }
    
    }
}
