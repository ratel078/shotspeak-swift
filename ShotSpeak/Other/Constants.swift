//
//  Constants.swift
//  ShotSpeak
//
//  Created by Sergiy Bekker on 15.11.17.
//  Copyright © 2017 SBApps. All rights reserved.
//

import UIKit

struct Constants {
    
    let addText =  "\nSwift — открытый мультипарадигмальный компилируемый язык программирования общего назначения. Создан компанией Apple в первую очередь для разработчиков iOS и macOS. Swift работает с фреймворками Cocoa и Cocoa Touch и совместим с основной кодовой базой Apple, написанной на Objective-C. Swift задумывался как более легкий для чтения и устойчивый к ошибкам программиста язык, нежели предшествовавший ему Objective-C[7]. Программы на Swift компилируются при помощи LLVM, входящей в интегрированную среду разработки Xcode 6 и выше. Swift может использовать рантайм Objective-C, что делает возможным использование обоих языков (а также С) в рамках одной программы "
    let selectCollor = UIColor(red: 12.0 / 255.0, green: 195.0 / 255.0, blue: 241.0 / 255.0, alpha: 1.0)
    let mainCollor = UIColor(red: 155.0 / 255.0, green: 155.0 / 255.0, blue: 155.0 / 255.0, alpha: 1.0)
    let unselectCollor = UIColor.lightGray
    
    let AUTH_DEFAULT =  ["email":"ratel078@gmail.com","password":"fazawaka","re_password":"fazawaka"]
    let HEADERS =  ["Content-Type": "application/x-www-form-urlencoded"]
    let BASE_URL : String = "http://kg.lampawork.com/api/v1.0/"
    
    func baseUrl(_ param:String) -> String
    {
        return "\(BASE_URL)\(param)"
    }
    
    func app_vesion() -> (String) {
        let nsObject: Any? = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as Any
        return nsObject as! String
    }
    func os_platrorm() -> (String) {
        return UIDevice().systemName
    }
    func os_version() -> (String) {
        return UIDevice().systemVersion
    }
    
    
}
