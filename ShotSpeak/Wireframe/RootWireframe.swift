//
//  RootWireframe.swift
//  koroche_govorya
//
//  Created by Sergiy Bekker on 16.11.17.
//  Copyright © 2017 SBApps. All rights reserved.
//

import UIKit

class RootWireframe: NSObject {

    func initWireframe () {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let navController = appDelegate.window?.rootViewController as! UINavigationController
        let controller = navController.viewControllers.first as! FeedController
        controller.attachPresenter(FeedPresenter.sharedInstance)
        FeedPresenter.sharedInstance.attachView(controller)
    }
}
