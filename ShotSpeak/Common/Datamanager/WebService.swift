//
//  WebService.swift
//  DiffcoTest
//
//  Created by Sergiy Bekker on 06.11.17.
//  Copyright © 2017 SBApps. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

struct Request {
    
    var access_token : String?
    var next : String?
    var headers : [String : String]!
    
}
enum appendUrl : String {
    case RegURL = "registration/"
    case LoginURL = "login/"
    case FeedURL = "feed/?ordering=?"
}

class WebService: NSObject {

    static let sharedInstance = WebService()
    
    var requestData : Request!

    override init() {
        requestData = Request()
       super.init()
    }
    
    func login(_ param:[String : Any], completion: @escaping (_ result: JSON) -> Void) {
  
        let request = Alamofire.request(Constants().baseUrl(String(appendUrl.LoginURL.rawValue)), method: HTTPMethod.post, parameters: param, encoding: JSONEncoding.default, headers: Constants().HEADERS)

        request.validate(statusCode: 200..<500).validate(contentType: ["application/json"]).responseJSON { response in

            switch(response.result) {
            case.success(let data):

                let json = JSON(data)
 
                let access_token = json["access_token"].rawValue
                self.requestData.access_token = access_token as? String
                self.requestData.headers =  ["authorization": "\(access_token)"]

                DispatchQueue.main.async {
                    completion(json)
                }

            case.failure(let error):
                print("Not Success",error)
            }
        }
        
     
    }
    
    func requestFeed(_ param:[String : Any], completion: @escaping (_ result: JSON) -> Void) {
     
        login(param, completion: {(_ result: JSON) in
           
   
            let url = self.requestData.next == nil ? Constants().baseUrl(String(appendUrl.FeedURL.rawValue)) :
           self.requestData.next
            let request = Alamofire.request(url!, method: HTTPMethod.get, parameters:nil, encoding: JSONEncoding.default, headers: self.requestData.headers)
            
            
            request.validate(statusCode: 200..<500).validate(contentType: ["application/json"]).responseJSON { response in
    
                switch(response.result) {
                    case.success(let data):
                        completion(JSON(data))
                    case.failure(let error):
                        print("Not Success",error)
                }
            }
        })
    }

   
}
