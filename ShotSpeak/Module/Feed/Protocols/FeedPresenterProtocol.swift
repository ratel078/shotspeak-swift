//
//  FeedPresenterProtocol.swift
//  koroche_govorya
//
//  Created by Sergiy Bekker on 16.11.17.
//  Copyright © 2017 SBApps. All rights reserved.
//

import UIKit

protocol FeedPresenterProtocol : NSObjectProtocol {
    func updateContent(_ content : FeedView)
}

