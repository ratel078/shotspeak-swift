//
//  FeedControllerProtocol.swift
//  koroche_govorya
//
//  Created by Sergiy Bekker on 16.11.17.
//  Copyright © 2017 SBApps. All rights reserved.
//

import UIKit

protocol FeedControllerProtocol : NSObjectProtocol {
    func feedContent(_ nextUrl : String?)
}
