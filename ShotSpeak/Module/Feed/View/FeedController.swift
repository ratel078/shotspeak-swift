//
//  ViewController.swift
//  ShotSpeak
//
//  Created by Sergiy Bekker on 10.11.17.
//  Copyright © 2017 SBApps. All rights reserved.
//

import UIKit
import SDWebImage
import QuickRippleButton


enum CellType : String {
    
    case FeeCell = "FeedCell"
    case ActivityCell = "ActivityCell"
    
    static let allValues = [FeeCell.rawValue]
}
enum CellSizeType : Float {
    case FeedSize = 532.0
    case FeedTextSize = 170.0
    case FeedImageSize = 150.0
    
    static let allValues = [FeedSize.rawValue]
}
enum WightType : Float {
    case Wieght_SE = 255
    case Wieght_8 = 300
    case Wieght_8Plus = 320
}


class FeedController: UIViewController {

    fileprivate var content = NSMutableArray()
    fileprivate var isInsert : Bool = false
    fileprivate var isScroll : Bool = false
    fileprivate var lastIndexPath : IndexPath! = IndexPath(row: 0, section: 0)
    @IBOutlet weak var table : UITableView!
    @IBOutlet weak var segment : SegmentView!
    weak var presenter: FeedPresenter?
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Новости"
        self.table.register(UINib(nibName: String(CellType.ActivityCell.rawValue), bundle: nil), forCellReuseIdentifier:String(CellType.ActivityCell.rawValue))
        
        segment.attachView(self)
        FeedPresenter.sharedInstance.attachView(self)
        presenter?.feedContent(nil)
        initNavbar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(FeedController.feedMoreNotification), name: .feedMoreNotification, object: nil)
     
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.removeObserver(self, name: .feedMoreNotification, object: nil)
    }

    func attachPresenter(_ obj:FeedControllerProtocol){
        self.detachPresenter()
        presenter = obj as? FeedPresenter
    }
    
    func detachPresenter() {
        presenter = nil
    }
    //MARK: - Notification methods

    @objc func feedMoreNotification(_ obj : Notification) {
        moreCell(obj)
    }
    
    //MARK: - Private methods
    
    fileprivate func initNavbar() {
        let btn1 = QuickRippleButton(type: .custom)
        var image  = UIImage(named: "menu")
        btn1.setImage(image, for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width:30.0, height: 30.0)
        btn1.addTarget(self, action: #selector(FeedController.clickeNavbar), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: btn1)
        
        let btn2 = QuickRippleButton(type: .custom)
        image  = UIImage(named: "add")
        btn2.setImage(image, for: .normal)
        btn2.frame = CGRect(x: 0, y: 0, width: 30.0, height: 30.0)
        btn2.addTarget(self, action: #selector(FeedController.clickeNavbar), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: btn2)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
    }
    
    @objc func clickeNavbar ( sender : UIButton)  {
        
    }
    
    fileprivate func sizeWithContent(_ indexPath : IndexPath) -> CGFloat {
       
        let model = self.content[indexPath.row] as! FeedModel
        var size = CGFloat(CellSizeType.FeedSize.rawValue)
        if model.image == nil {
            size = size - CGFloat(CellSizeType.FeedImageSize.rawValue)
        }
        if model.text != nil {
            let text = model.text! as String
            let hieght = self.calculateHeight(inString:text,width:CGFloat(self.boundWight().rawValue))
            if hieght < CGFloat(CellSizeType.FeedTextSize.rawValue){
                size = size - (CGFloat(CellSizeType.FeedTextSize.rawValue) - hieght)
            } else {
                if model.isMore {
                    size = size + hieght - CGFloat(CellSizeType.FeedTextSize.rawValue)
                }
            }
        }

        return size
    }
    
    fileprivate func moreCell(_ obj: Notification) {
        let index = obj.object! as! Int
        var model = self.content[index] as! FeedModel
        model.isMore = !model.isMore
        self.content.replaceObject(at: index, with: model)
        var position : UITableViewScrollPosition = UITableViewScrollPosition.bottom
        if !model.isMore{
            position = UITableViewScrollPosition.none
        }
        self.table.reloadVisibleCells([IndexPath(row: index, section: 0)], completion: {
              self.table.scrollToRow(at: IndexPath(row: index, section: 0), at: position, animated: false)
        })

    }
}

//MARK: - UITableView dataSource

extension FeedController: UITableViewDataSource {
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return content.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : UITableViewCell
        
        if indexPath.row == self.content.count - 1  && self.isInsert {
            let Cell = tableView.dequeueReusableCell(withIdentifier: String(CellType.ActivityCell.rawValue), for: indexPath) as! ActivityCell
            Cell.startAnime()
            cell = Cell
        } else {
            let Cell = tableView.dequeueReusableCell(withIdentifier: String(CellType.FeeCell.rawValue), for: indexPath) as! FeedCell
            Cell.index = indexPath.row
            Cell.content = (self.content[indexPath.row]) as? FeedModel
            
            cell = Cell
        }
        
        return cell
    }

    internal func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        cell.layoutMargins = UIEdgeInsets.zero
        cell.separatorInset = UIEdgeInsets.zero
        
    }
}

//MARK: - UITableView delegate

extension FeedController: UITableViewDelegate {
    
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == self.content.count - 1  && self.isInsert {
            return 60.0
        }
        
        return self.sizeWithContent(indexPath)
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let model = self.content[indexPath.row] as! FeedModel
        self.alertMessage("", "Вы нажали на тему \(String(describing: model.id!))")
    }
}

//MARK: - UIScrollView delegate

extension FeedController: UIScrollViewDelegate {
    
    internal func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        
        self.perform(#selector(scrollViewDidEndScrollingAnimation(_:)), with: self, afterDelay: 0.03)
    }

    internal func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView)
    {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        
        if table.contentOffset.y >= table.contentSize.height - table.frame.size.height - 10.0 {
            let obj = self.content[self.content.count - 2] as! FeedModel
            if obj.next != nil && isInsert{
                isInsert = false
                presenter?.feedContent(obj.next!)
            }
        }
        
    }
}


//MARK: - Segment protocol

extension FeedController: SegmentDelegate{
    func selectItem(_ obj : SegmentView, _ index : Int){
        self.table.isHidden = index > 0
    }
}

//MARK: - FeedPresenter protocol

extension FeedController: FeedPresenterProtocol{

    func updateContent(_ param : FeedView){
        
        if self.content.count  > 0 {
            self.content.removeObject(at:(self.content.count - 1))
            self.lastIndexPath = IndexPath(row: self.content.count, section: 0)
        }
        self.content.addObjects(from:param.feedData)
        let obj = self.content[self.content.count - 1] as! FeedModel
        if obj.next != nil {
            self.content.add(FeedModel([:],"",0))
        }
        
        self.table.dataSource = self
        self.table.delegate = self
        
        UIView.setAnimationsEnabled(false)
        self.table.reloadData(){
            self.isInsert = obj.next != nil
            self.table.scrollToRow(at: self.lastIndexPath, at: UITableViewScrollPosition.none, animated: false)
            UIView.setAnimationsEnabled(true)
        }
        
    }
}
